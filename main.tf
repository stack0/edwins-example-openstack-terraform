terraform {
  required_providers {
    openstack-auto-topology = {
      source  = "terraform.cyverse.org/cyverse/openstack-auto-topology"
    }
    openstack = {
      source = "terraform-provider-openstack/openstack" # "terraform.cyverse.org/cyverse/openstack"
    }
  }
}

provider "openstack" {
  tenant_name = var.project
}

provider "openstack-auto-topology" {}

data "openstack-auto-topology_auto_allocated_topology" "network" {}

output "network_id" {
  value = data.openstack-auto-topology_auto_allocated_topology.network.id
}

resource "openstack_compute_instance_v2" "os_instances" {
  name = "${var.instance_name}${count.index}"
  count = 3
  image_name = "Featured-Ubuntu22"
  flavor_name = var.flavor
  key_pair = var.keypair
  security_groups = ["cacao-default"]
  power_state = var.power_state
  user_data = var.user_data

  network {
    # name = "auto_allocated_network"
    uuid = data.openstack-auto-topology_auto_allocated_topology.network.id
  }

  lifecycle {
    ignore_changes = [
      image_id
    ]
  }
}

resource "openstack_networking_floatingip_v2" "os_floatingips" {
  count = 3
  pool = var.ip_pool
  description = "floating ip for ${var.instance_name}, ${count.index}/3"
}

resource "openstack_compute_floatingip_associate_v2" "os_floatingips_associate" {
  count = 3
  floating_ip = openstack_networking_floatingip_v2.os_floatingips[count.index].address
  instance_id = openstack_compute_instance_v2.os_instances[count.index].id
}
